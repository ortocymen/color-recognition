#include <stdio.h>
#include <stdlib.h>
#include "search.h"


/*
Creates a table into file with given struct name
In all direction it makes always steps + 1 samples, which are written into file (thus we have steps + 1 on power of 3 samples in file)
It is necessary to have steps of power of 2 (1, 2, 4, 8....) or 3,5 or 17

Two types of created table:
ones with 3,7 or 15 steps which have constant length of one step
ones with step of power of 2 which have constant length of step except for the last one, which has to be shorter by 1 to include into set white too
*/
int regular_table_creator(const char* file_name, const char* struct_name, int step_len, rgb *table, int table_len) {
    FILE *f = fopen(file_name,"w");
    if(f == NULL)
        return 1;
    rgb random_color, closest_color;
    int steps = (255/step_len < 255.0/step_len) ? 255/step_len + 1 : 255/step_len;

    fprintf(f,"#include \"rgb_func.h\"\n\nrgb ");
    fprintf(f, struct_name);
    fprintf(f, "[] = {\n");
    for(int i = 0; i <= steps; i++) {
        random_color.data[0]  = (i*step_len > 255 ? 255 : i*step_len);

        for(int o = 0; o <= steps; o++) {
            random_color.data[1]  = (o*step_len > 255 ? 255 : o*step_len);

            for(int p = 0; p <= steps; p++) {
                random_color.data[2] =(p*step_len > 255 ? 255 : p*step_len);
                closest_color = linear_search(random_color, table, table_len);
                fprintf(f, "\t{\"");
                fprintf(f, closest_color.text);
                fprintf(f, "\", ");
                char str[15];
                sprintf(str, "%d", random_color.data[0]);
                fprintf(f, str);
                fprintf(f, ", ");
                sprintf(str, "%d", random_color.data[1]);
                fprintf(f, str);
                fprintf(f, ", ");
                sprintf(str, "%d", random_color.data[2]);
                fprintf(f, str);
                fprintf(f, "},\n");

            }
        }
    }
    fprintf(f, "};");
    fclose(f);
    return 0;
}

int rewrite_table(const char* file_name, rgb *table, int table_len) {
    FILE *f = fopen(file_name,"w");
    if(f == NULL)
        return 1;
    fprintf(f,"#include \"rgb_func.h\"\n\nrgb big_table[] = {\n");
    for(int i = 0; i < table_len; i++) {
        fprintf(f,"\t{\"%s\", {%d,%d,%d}},\n", table[i].text,table[i].data[0],table[i].data[1],table[i].data[2]);
    }
    fprintf(f,"};");
    fclose(f);
    return 0;
}
