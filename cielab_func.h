#ifndef COLOR_SPACE_CONVERTOR_H_INCLUDED
#define COLOR_SPACE_CONVERTOR_H_INCLUDED

#include "rgb_func.h"

typedef struct {
    char *text;
    double l;
    double a;
    double b;
} cielab;

double cielab_eucl_distance(cielab point1, cielab point2);
cielab rgb_to_cielab(rgb rgb_color);


#endif // COLOR_SPACE_CONVERTOR_H_INCLUDED
