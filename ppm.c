#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <strings.h>

#include "rgb_func.h"

/**
* Checks if the file has the right header as a PPM image
* @param fp File handler, has to be open and valid!
* @param dinx Wight of image, returned value
* @param diny Height of image, returned value
* @return Number 0 if operation was successful and value != 0 if not
*/
int check_ppm(FILE *fp, int *dimx, int *dimy) {
    *dimx = *dimy = 0;
    char buff[4];

// A "magic number" for identifying the file type. A ppm image's magic number is the two characters "P6".
    fgets(buff, 3, fp);
    if((buff == NULL) || (strncmp(buff, "P6", 2))) {
        fclose(fp);
        return 2;
    }

// Whitespace (blanks, TABs, CRs, LFs).
    int sign;
    if(isspace((sign = fgetc(fp))) == 0) {
        fclose(fp);
        return 2;
    }
    while(isspace((sign = fgetc(fp))) != 0)
        ;
    ungetc(sign, fp);

// A width, formatted as ASCII characters in decimal.
    while((isspace(sign = fgetc(fp))) == 0)
    {
        if(isdigit(sign) == 0)
        {
            fclose(fp);
            return 2;
        }
        *dimx = 10*(*dimx) + sign - '0';
    }
    ungetc(sign,fp);

// Whitespace.
    if(isspace((sign = fgetc(fp))) == 0) {
        fclose(fp);
        return 2;
    }
    while(isspace((sign = fgetc(fp))) != 0)
        ;
    ungetc(sign, fp);

// A height, again in ASCII decimal.
    while((isspace(sign = fgetc(fp))) == 0)
    {
        if(isdigit(sign) == 0)
        {
            fclose(fp);
            return 2;
        }
        *dimy = 10*(*dimy) + sign - '0';
    }
    ungetc(sign,fp);

// Whitespace.
    if(isspace((sign = fgetc(fp))) == 0) {
        fclose(fp);
        return 2;
    }
    while(isspace((sign = fgetc(fp))) != 0)
        ;
    ungetc(sign, fp);

// The value 255.
    fgets(buff, 4, fp);
    if(strncmp(buff,"255", 3) != 0)
    {
        fclose(fp);
        return 2;
    }

// A single whitespace character (usually a newline).
    if(isspace((sign = fgetc(fp))) == 0) {
        fclose(fp);
        return 2;
    }

    return 0; // everything OK
}

/*
* Finds pixel exactly in the middle of image and return its value
*/
rgb find_middle_pixel(FILE *fp, int dimx, int dimy) {
    char tmp[4];
    fseek(fp, 3*(dimy*dimx/2 + dimx/2), SEEK_CUR);
    fgets(tmp, 4, fp);
    rgb color = {NULL, {tmp[0], tmp[1], tmp[2]}};
    return color;
}
