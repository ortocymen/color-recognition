#include <stdlib.h>
#include <math.h>
#include <strings.h>
#include <stdio.h>

#include "rgb_func.h"
#include "search.h"

/*
Naive approach - linear search through all data
Looks for the color RGB with smallest Euclidean distance in 3D space defined by axis r,g and b
Calculate all possible color points in table and gives the smallest
It is necessary to have in the end of table rgb for white color etc. r = g = b = 255
*/
rgb linear_search(rgb color, rgb *table, int len) {
    double min = INFINITY;
    double dist;
    int min_index = 0;

    for(int i = 0; i < len; i++){
        dist = rgb_weighted_distance(color, table[i]);
        if(dist < min) {
            min = dist;
            min_index = i;
        }
    }
    return table[min_index];
}

/*
Table approach
Needs to have prepared and ordered table
*/
rgb table_search(rgb color, rgb *table, int step_len) {
    int r, g, b;
    int r_o, g_o, b_o;

    if(color.data[0]%step_len > step_len/2) {
        r = color.data[0] + (step_len - color.data[0]%step_len);
        if(r > 255) {
            r = 255;
            r_o = r/step_len + 1;
        }
        else
            r_o = r/step_len;
    }
    else {
        r = color.data[0] - color.data[0]%step_len;
        r_o = r/step_len;
    }
    if(color.data[1]%step_len > step_len/2) {
        g = color.data[1] + (step_len - color.data[1]%step_len);
        if(g > 255) {
            g = 255;
            g_o = g/step_len + 1;
        }
        else
            g_o = g/step_len;
    }
    else {
        g = color.data[1] - color.data[1]%step_len;
        g_o = g/step_len;
    }
    if(color.data[2]%step_len > step_len/2) {
        b = color.data[2] + (step_len - color.data[2]%step_len);
        if(b > 255) {
            b = 255;
            b_o = b/step_len + 1;
        }
        else
            b_o = b/step_len;
    }
    else {
        b = color.data[2] - color.data[2]%step_len;
        b_o = b/step_len;
    }

    int dimension = (255/step_len < 255.0/step_len) ? 255/step_len + 2 : 255/step_len + 1; // number of points in each direction
    return table[b_o + dimension*g_o + dimension*dimension*r_o];

}

/*
Copies data from one node to another
*/
void copy_kd_node(kd_node *to, kd_node *from) {
    to->color = from->color;
    to->left = from->left;
    to->right = from->right;
}

/*
Swaps to nodes using temporary node
*/
void swap_kd_node(kd_node *node1, kd_node *node2) {
    kd_node tmp;
    copy_kd_node(&tmp, node1);
    copy_kd_node(node1, node2);
    copy_kd_node(node2, &tmp);
}

/**
* Finds a k-dimensional node which value is in the middle (length of table / 2) of all values in array using Quick Select
* @param nodes Array of nodes
* @param len_table Length of table, has to be higher than 0
* @param choose Chooses the axis, upon which we sort
* @return The found kn node, whose value is in the middle
*/
kd_node* find_median(kd_node *nodes, int len_table, int choose) {
    int val = 0, curr = 0, end = len_table, start = 0;
    int pivot;

    while(1) {
        pivot = nodes[end - 1].color->data[choose];
        for( ; curr < end; curr++){
            if(nodes[curr].color->data[choose] < pivot) {
                if(val != curr)
                    swap_kd_node(&nodes[curr], &nodes[val]);
                val++;
            }
        }
        swap_kd_node(&nodes[val], &nodes[end - 1]);

        if(val == len_table/2) {
            return &nodes[val];
        }

        if(val > len_table/2) { // val is lower than the middle, we search in the left part
            end = val;
            curr = val = start;
        }
        else {
            start = curr = ++val; // val is above the middle, we has to search in the right part of array
        }
    }
}

/*
Creates kd array by allocating place and setting pointers
*/
kd_node* create_kd_array(rgb *table, int len) {
    kd_node* kd_array = malloc(sizeof(kd_node)*len);
    for(int i = 0; i < len; i++) {
        kd_array[i].color = &table[i];
    }
    return kd_array;
}

/*
Frees allocated memory
*/
void destroy_kd_array(kd_node *kd_array) {
    free(kd_array);
    kd_array = NULL;
}

/*
Recursively creates kd tree - finds the middle of current axis and call itself to self and right subtrees
*/
kd_node* create_kd_tree(kd_node *nodes, int len, int choose) {
    if(len == 0)
        return NULL;
    choose %= 3;

    kd_node *curr_node;
    if((curr_node = find_median(nodes, len, choose))) {
        curr_node->left  = create_kd_tree(nodes, curr_node - nodes, choose + 1);
        curr_node->right = create_kd_tree(curr_node + 1, (nodes + len) - (curr_node +1), choose + 1);
    }
    return curr_node;

}

/**
* Finding in k-dimensional tree done recursively - uses Euclidean distance
* @param root_node Pointer to root of kd tree
* @param target The color we seek
* @param closest_node The current closest node, which we found so far. When called set pointer to NULL!
* @param choose The axis, by which we cut the tree
**/
void kd_tree_search(kd_node *root_node, rgb *target, kd_node **closest_node, double *closest_distance, int choose) {
    if(root_node != NULL) {
        choose %= 3;
        double curr_dist  = rgb_eucl_distance(*root_node->color, *target);
        if(!*closest_node || *closest_distance > curr_dist) {
            *closest_node     = root_node;
            *closest_distance = curr_dist;
        }

        int axis_dist = root_node->color->data[choose] - target->data[choose];
        kd_tree_search(axis_dist > 0 ? root_node->left : root_node->right, target, closest_node, closest_distance, choose + 1);
        if(*closest_distance >= axis_dist*axis_dist) {
            /*
            * To prune the tree we do not call func upon second subtree if point is further than axis_dist from cutting line.
            * It makes no sense to look further, because the closest distance is smaller than distance from axis,
            *   thus no point from that area can be closer than the current closest distance.
            * We use a second power of axis_dist because in Euclidean distance we did not use square root of result
            */
            kd_tree_search(axis_dist > 0 ? root_node->right : root_node->left, target, closest_node, closest_distance, choose + 1);
        }
    }
}
