#ifndef PPM_H_INCLUDED
#define PPM_H_INCLUDED

int check_ppm(FILE *fp, int *dimx, int *dimy);
rgb find_middle_pixel(FILE *fp, int dimx, int dimy);

#endif // PPM_H_INCLUDED
