#include <math.h>
#include "cielab_func.h"


/*
Distance of two CIE L*ab points
Euclidean distance, but without square root
*/
double cielab_eucl_distance(cielab point1, cielab point2) {
    return (point1.l - point2.l)*(point1.l - point2.l) + (point1.a - point2.a)*(point1.a - point2.a)
                 + (point1.b - point2.b)*(point1.b - point2.b);
}

/*
Convert RGB into CIE L*ab
Mathematical definitions taken from: http://www.brucelindbloom.com/
*/
cielab rgb_to_cielab(rgb rgb_color) {
    double r = rgb_color.data[0] / 255.0;
    double g = rgb_color.data[1] / 255.0;
    double b = rgb_color.data[2] / 255.0;

    if( r > 0.04045) {
        r = pow((r + 0.055 ) / 1.055, 2.4);
    }
    else {
        r = r / 12.92;
    }
    if( g > 0.04045) {
        g = pow((g + 0.055 ) / 1.055, 2.4);
    }
    else {
        g = g / 12.92;
    }
    if( b > 0.04045) {
        b = pow((b + 0.055 ) / 1.055, 2.4);
    }
    else {
        b = b / 12.92;
    }
    r *= 100.0;
    g *= 100.0;
    b *= 100.0;

    double x = (r * 0.4124 + g * 0.3576 + b * 0.1805) / 95.047;
    double y = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 100.000;
    double z = (r * 0.0193 + g * 0.1192 + b * 0.9505) / 108.883;


    if( x > 0.008856) {
        x = pow(x, 0.3333333333);
    }
    else {
        x = (7.787 * x) + (16 / 116.0);
    }
    if( y > 0.008856) {
        y = pow(y, 0.33333333333);
    }
    else {
        y = (7.787 * y) + (16 / 116.0);
    }
    if( z > 0.008856) {
        z = pow(z, 0.3333333333333333);
    }
    else {
        z = (7.787 * z) + (16 / 116.0);
    }

    cielab lab_color;
    lab_color.text = rgb_color.text;
    lab_color.l = ( 116.0 * y ) - 16.0;
    lab_color.a = 500.0 * ( x - y );
    lab_color.b = 200.0 * ( y - z );

    return lab_color;
}
