#include "rgb_func.h"

/*
Count Euclidean distance by giving difference but without square root
*/
double rgb_eucl_distance(rgb point1, rgb point2) {
    return ((point1.data[0] - point2.data[0])*(point1.data[0] - point2.data[0]) + (point1.data[1] - point2.data[1])*(point1.data[1] - point2.data[1])
                 + (point1.data[2] - point2.data[2])*(point1.data[2] - point2.data[2]));
}

/*
Count Euclidean distance by giving difference but without square root
*/
double arr_eucl_distance(unsigned char point1[3], unsigned char point2[3]) {
    return ((point1[0] - point2[0])*(point1[0] - point2[0]) + (point1[1] - point2[1])*(point1[1] - point2[1])
                 + (point1[2] - point2[2])*(point1[2] - point2[2]));
}

/*
Counts weighted distance of RGB colors, better appropriated to human sight than Euclidean distance of two RGB colors

Author: Thiadmer Riemersma
Source: http://www.compuphase.com/cmetric.htm
Licence: Creative commons 3.
Changes: No changes were made.
*/
double rgb_weighted_distance(rgb point1, rgb point2) {
  long rmean = ( (long)point1.data[0] + (long)point2.data[0] ) / 2;
  long r = (long)point1.data[0] - (long)point2.data[0];
  long g = (long)point1.data[1] - (long)point2.data[1];
  long b = (long)point1.data[2] - (long)point2.data[2];
  return (((512+rmean)*r*r)>>8) + 4*g*g + (((767-rmean)*b*b)>>8);
}
