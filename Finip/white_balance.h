#ifndef WHITE_BALANCE_H_INCLUDED
#define WHITE_BALANCE_H_INCLUDED

#include <gdk-pixbuf/gdk-pixbuf.h>

GdkPixbuf *color_balance(GdkPixbuf *pixbuf);
GdkPixbuf *white_balance(GdkPixbuf *pixbuf);
GdkPixbuf *color_area(GdkPixbuf *pixbuf, int x, int y);

#endif //#ifndef WHITE_BALANCE_H_INCLUDED
