#ifndef SEARCH_H_INCLUDED
#define SEARCH_H_INCLUDED

#include "rgb_func.h"

typedef struct kd_node_t {
    named_rgb *color;
    struct kd_node_t *left, *right;
} kd_node;

void swap(kd_node *node1, kd_node *node2);
kd_node* find_median(kd_node *nodes, int len_table, int choose);

named_rgb linear_search(rgb color, named_rgb *table, int len);
named_rgb table_search(rgb color, named_rgb *table, int step_len);
kd_node* create_kd_array(named_rgb *table, int len);
void destroy_kd_array(kd_node *kd_array);
kd_node* create_kd_tree(kd_node *nodes, int len, int choose);
//void kd_tree_recursive(kd_node *root_node, rgb *target, kd_node **closest_node, double *closest_distance, int choose)
kd_node* kd_tree_search(kd_node *root_node, rgb *target);

#endif // SEARCH_H_INCLUDED
