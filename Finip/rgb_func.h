#ifndef RGB_FUNC_H_INCLUDED
#define RGB_FUNC_H_INCLUDED

typedef struct {
    unsigned char data[3];
} rgb;

typedef struct {
	char *detail;
	char *tone;
	rgb color;
} named_rgb;

double rgb_eucl_distance(rgb point1, rgb point2);
double arr_eucl_distance(unsigned char point1[3], unsigned char point2[3]);
double rgb_weighted_distance(rgb point1, rgb point2);

#endif // RGB_FUNC_H_INCLUDED
