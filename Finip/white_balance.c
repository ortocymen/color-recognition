#include <stdlib.h>


#include "rgb_func.h"
#include "search.h"
#include "white_balance.h"

#include <gdk-pixbuf/gdk-pixbuf.h>

#define SATURATION 0.035 // Saturation of picture, recommended below 5% (=0.05)

/**
* @brief Simple automatic color balance
* Color balance provides simple color correction - saturates 5% pixels on both extremes and others are rescaled accordingly
* @param pixbuf Pointer to a valid structure GdkPixbuf, the basic structure of library
*
* @return Structure with balance data
**/
GdkPixbuf *color_balance(GdkPixbuf *pixbuf) {
    g_assert(gdk_pixbuf_get_colorspace (pixbuf) == GDK_COLORSPACE_RGB);
    g_assert(gdk_pixbuf_get_bits_per_sample (pixbuf) == 8);

    guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
    int n_channels = gdk_pixbuf_get_n_channels(pixbuf); // TODO: check multiple n channels and accept only 3 and 4
    int width = gdk_pixbuf_get_width (pixbuf);
    int height = gdk_pixbuf_get_height (pixbuf);

    long hist_red[256] = {0,};
    long hist_green[256] = {0,};
    long hist_blue[256] = {0,};

    int total_size = width*height;
    for(int i = 0; i < total_size; i++) { // Histogram of values
        hist_red[pixels[n_channels*i]]++;
        hist_green[pixels[n_channels*i+1]]++;
        hist_blue[pixels[n_channels*i+2]]++;
    }

    for(int i = 1; i <= 255; i++) { // Cumulative histogram  
	hist_red[i]     = hist_red[i] + hist_red[i-1];
        hist_green[i]   += hist_green[i-1];
        hist_blue[i]    += hist_blue[i-1];
    }

// Red channel
    int v_min_red = 0;
    while(hist_red[v_min_red+1] <= total_size*SATURATION) {
        v_min_red++;
    }

    int v_max_red = 254;
    while(hist_red[v_max_red-1] > total_size*(1-SATURATION)) {
        v_max_red--;
    }

/*----------------------------------------------------------------------------------------------------------------------*/
// Green channel
    int v_min_green = 0;
    while(hist_red[v_min_green+1] <= total_size*SATURATION) {
        v_min_green++;
    }
    int v_max_green = 254;
    while(hist_red[v_max_green-1] > total_size*(1-SATURATION)) {
        v_max_green--;
    }
/*----------------------------------------------------------------------------------------------------------------------*/
// Blue channel
    int v_min_blue = 0;
    while(hist_red[v_min_blue+1] <= total_size*SATURATION) {
        v_min_blue++;
    }
    int v_max_blue = 254;
    while(hist_red[v_max_blue-1] > total_size*(1-SATURATION)) {
        v_max_blue--;
    }

    float red_correction = 255.0/(v_max_red-v_min_red);
    float green_correction = 255.0/(v_max_green-v_min_green);
    float blue_correction = 255.0/(v_max_blue-v_min_blue);

    for(int i = 0; i<total_size; i++){
// Red channel
        pixels[n_channels*i] = (pixels[n_channels*i] < v_min_red) ? 0 :
          ((pixels[n_channels*i] > v_max_red) ? 255 : (pixels[n_channels*i] - v_min_red) * red_correction);
/*----------------------------------------------------------------------------------------------------------------------*/
// Green channel
        pixels[n_channels*i + 1] = (pixels[n_channels*i+1] < v_min_green) ? 0 :
          ((pixels[n_channels*i+1] > v_max_green) ? 255 : (pixels[n_channels*i+1] - v_min_green) * green_correction);
/*----------------------------------------------------------------------------------------------------------------------*/
// Blue channel
        pixels[n_channels*i + 2] = (pixels[n_channels*i+2] < v_min_blue) ? 0 :
          ((pixels[n_channels*i+2] > v_max_blue) ? 255 : (pixels[n_channels*i+2] - v_min_blue ) * blue_correction);
    }

    return pixbuf;
}


/**
* @brief Simple white balance
*
* @param pixbuf Pointer to a valid structure GdkPixbuf, the basic structure of library
* @param white_to_be Pointer to pixel considered to be a white in reality
* @return
**/
GdkPixbuf *white_balance(GdkPixbuf *pixbuf) {
    g_assert(gdk_pixbuf_get_colorspace (pixbuf) == GDK_COLORSPACE_RGB);
    g_assert(gdk_pixbuf_get_bits_per_sample (pixbuf) == 8);


    guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
    int n_channels = gdk_pixbuf_get_n_channels(pixbuf); // TODO: check multiple n channels and accept only 3 and 4
    int width = gdk_pixbuf_get_width (pixbuf);
    int height = gdk_pixbuf_get_height (pixbuf);
    int total_size = width*height;

	guchar *white_to_be;
	char white[3] = {255,255,255}; 
	double min_dist = 10000;
	double dist;
    for(int i = 0; i < total_size; i++) {
        if((dist = arr_eucl_distance(&pixels[n_channels*i], white)) < min_dist) {
		min_dist = dist;
		white_to_be = &pixels[n_channels*i];
printf("%d %d %d\n", white_to_be[0], white_to_be[1], white_to_be[2]);
	}
    }


    float red_correction = 255.0/white_to_be[0];
    float green_correction = 255.0/white_to_be[1];
    float blue_correction = 255.0/white_to_be[2];



    for(int i = 0; i < total_size; i++) {
        pixels[n_channels*i  ] *= red_correction;
        pixels[n_channels*i+1] *= green_correction;
        pixels[n_channels*i+2] *= blue_correction;
    }
    return pixbuf;
}

int *sum_rgb(int *a, guchar *b) { //TODO: make a macro or in-line function
    a[0] += b[0];
    a[1] += b[1];
    a[2] += b[2];
    return a;
}

/**
* @brief Finds the area with the same color as given pixel
* Finds the area with the same color as a pixel defined by its' coordinates
* It makes copy of pixbuf, smooth all area by averaging pixel value
* @param pixbuf Pointer to a valid structure GdkPixbuf, the basic structure of library
* @param x X coordinate of pixel
* @param y Y coordinate of pixel
* @return New pixbuf with marked area
**/
GdkPixbuf *color_area(GdkPixbuf *pixbuf, int x, int y) {
    g_assert(gdk_pixbuf_get_colorspace (pixbuf) == GDK_COLORSPACE_RGB);
    g_assert(gdk_pixbuf_get_bits_per_sample (pixbuf) == 8);

    GdkPixbuf *new_pixbuf = gdk_pixbuf_copy(pixbuf);
    guchar *pixels = gdk_pixbuf_get_pixels(new_pixbuf);
    int n_channels = gdk_pixbuf_get_n_channels(pixbuf); // TODO: check multiple n channels and accept only 3 and 4

    int width = gdk_pixbuf_get_width (pixbuf);
    int height = gdk_pixbuf_get_height (pixbuf);
    int total_size = width*height;
    int rowstride = gdk_pixbuf_get_rowstride (pixbuf);

int *sum = malloc(3*sizeof(int));


printf("%d %d %d %d\n", pixels[20*rowstride + 60*n_channels], pixels[21*rowstride + 60*n_channels], pixels[22*rowstride + 60*n_channels], pixels[23*rowstride + 60*n_channels]);
/*
Unification and smoothening of picture
*/
    for(int i = 0; i < 10/*height*/; i++) {
        for(int q = 0; q < 10/*width*/; q++) {
            sum[0] = pixels[i*rowstride + q*n_channels    ];
            sum[1] = pixels[i*rowstride + q*n_channels + 1];
            sum[2] = pixels[i*rowstride + q*n_channels + 2];
            sum = (i == 0) ? sum_rgb(sum, &pixels[i*rowstride + q*n_channels ]) : sum_rgb(sum, &pixels[(i-1)*rowstride + q*n_channels]);
            sum = (i == height-1) ? sum_rgb(sum, &pixels[i*rowstride + q*n_channels]) : sum_rgb(sum, &pixels[(i+1)*rowstride + q*n_channels]);
            sum = (q == 0) ? sum_rgb(sum, &pixels[i*rowstride + q*n_channels]) : sum_rgb(sum, &pixels[i*rowstride + (q-1)*n_channels]);
            sum = (q == width-1) ? sum_rgb(sum, &pixels[i*rowstride + q*n_channels]) : sum_rgb(sum, &pixels[i*rowstride + (q+1)*n_channels]);
            pixels[i*rowstride + q*n_channels  ] = sum[0]/5;
            pixels[i*rowstride + q*n_channels+1] = sum[1]/5;
            pixels[i*rowstride + q*n_channels+2] = sum[2]/5;
        }
    }

printf("%d %d %d %d\n", pixels[20*rowstride + 60*n_channels], pixels[21*rowstride + 60*n_channels], pixels[22*rowstride + 60*n_channels], pixels[23*rowstride + 60*n_channels]);

free(sum);

	return new_pixbuf;

	//TODO
}
