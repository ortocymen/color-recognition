#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <math.h>

#include "table.h"
#include "rgb_func.h"
#include "search.h"
#include "white_balance.h"

#include <gdk-pixbuf/gdk-pixbuf.h>


int main(int argc, char**argv)
{
	char *filename;
	if(argc != 2) {
		fprintf (stdout, "With which file am I supposed to work with? Please, tell me: ");
		filename = malloc(60);
		scanf("%s",filename);		
	}
	else {
		filename = argv[1];
	}

	GError *err = NULL;
	GdkPixbuf *p = gdk_pixbuf_new_from_file(filename, &err);
	if(p == NULL) {
		fprintf (stdout, "Error while loading file: %s\n", err->message);
		return 1;
	}

	p=color_balance(p);


	//rgb barva = {30, 250, 248};
	p = white_balance(p);

	if(p)
		gdk_pixbuf_save(p, "new.jpg", "jpeg", &err, "quality",  "70", NULL);		

	return 0;
}
