#ifndef RGB_FUNC_H_INCLUDED
#define RGB_FUNC_H_INCLUDED

typedef struct {
    char *text;
    unsigned char data[3];
} rgb;

double rgb_eucl_distance(rgb point1, rgb point2);
double rgb_weighted_distance(rgb point1, rgb point2);

#endif // RGB_FUNC_H_INCLUDED
