#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include "regular_table.h"

#include "table.h"
#include "rgb_func.h"
#include "search.h"
#include "auxiliar.h"
#include "kd_tree.h"



int main()
{
    /*
    int step_len = 8;
    rgb* tab = big_table;
    int len_table = sizeof(big_table)/sizeof(rgb);
*/
#if 0
    regular_table_creator("regular_table.h", "table_of_colors" , step_len, tab, len_table);
#endif
#if 0
    rgb r = {"", 0, 0, 0};
    rgb a, b;
    int rozdil = 0, celkem = 0;
    for(int q = 0; q < 255; q  += 3) {
        r.r = q;
        for(int w = 0; w < 255; w += 3) {
            r.g = w;
            for(int i = 0; i < 255; i += 3) {
                r.b = i;
                a = linear_search(r, tab);
                b = table_search(r, table_of_colors, step_len);
                if(strcmp(a.text, b.text) != 0)
                    rozdil++;
                celkem++;
            }
        }
    }
    printf("Chyb celkem: %d (v %: %f) v %d vzorcich", rozdil, 100.0*rozdil/celkem, celkem);
#endif
    printf("%d", sizeof(big_table)/sizeof(rgb));
    return 0;
}
